%global commit dbe241f4c5310dd9bf3b451c538d78c6c4a0e288
%global shortcommit %(c=%{commit}; echo ${c:0:7})

%global         checkout 20130124git%{shortcommit}

Name:           raphael
Version:        2.1.0
Release:        1.%{checkout}%{?dist}
Summary:        JavaScript Vector Library
Group:          Applications/Internet
License:        MIT
URL:            http://raphaeljs.com/
Source0:        https://github.com/DmitryBaranovskiy/raphael/archive/%{commit}/%{name}-%{version}-%{shortcommit}.tar.gz
Source1:        %{name}.conf
BuildArch:      noarch

#BuildRequires:  
Requires:       httpd

%description
Raphaël is a small JavaScript library that should simplify your work
with vector graphics on the web. If you want to create your own specific
chart or image crop and rotate widget, for example, you can achieve it
simply and easily with this library.


%prep
%setup -q -n %{name}-%{commit}

%build

%install
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -p raphael.js  $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -p raphael-min.js $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -p -r plugins  $RPM_BUILD_ROOT%{_datadir}/%{name}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d
install -p -m 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d

%files
%doc README.markdown license.txt reference.html
%{_datadir}/%{name}
%config(noreplace) %{_sysconfdir}/httpd/conf.d/%{name}.conf

%changelog
* Fri Jan 24 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.1.0-1.20130124gitdbe241f
- Initial packaging
